package website.quan42.equaltowers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;



class TowersPart {

    ArrayList<Tower> towers;
    private ArrayList<Float> dividersVertices;
    private float dividerWidth;
    static float height;
    static int maxRemaining;
    static final int INVALID = 0;
    static final int VALID = 1;
    static final int EXACT = 2;
    private float startingY;

    TowersPart init(float height) {

        TowersPart.height = height;
        startingY = GameScreen.height - height;

        int t = Constants.NUMBER_OF_TOWERS;
        float dr = Constants.DIVIDER_RATIO_TO_TOWER_WIDTH;
        float towerWidth = (GameScreen.width / (t + ((t + 1f) * dr * 2f)));
        float s = (towerWidth * dr) * 2f;

        //generate towers
        towers = new ArrayList<Tower>();
        for (int i=0; i<t; i++ ) {
            float c = (s + towerWidth/2f)* (i+1) + (i * (towerWidth /2f));
            towers.add(new Tower().init(c, towerWidth, startingY));
        }
        calcMaxRemaining();

        dividerWidth = s/2f;
        dividersVertices = new ArrayList<Float>();
        for (int i=0; i<t+1; i++) {
            float c = (s/2f + i * (towerWidth + s)) - dividerWidth/2f;
            dividersVertices.add(c);
        }
        return this;
    }
    int checkClick(float clickX, int currentNumber) {
        for (Tower t : towers) {
            float x1 = t.position.x;
            float x2 = x1 + t.size.x;
            if ((clickX > x2 || clickX < x1)) continue;

            //from here the click is in this tower range.
            int remaining = t.limit - t.number;
            if (currentNumber > remaining) {
                t.animateInvalidClick();
                return INVALID;
            }
            if (currentNumber < remaining) {
                t.increaseNumber(currentNumber + t.number);
                calcMaxRemaining();
                return VALID;
            }
            if (currentNumber == remaining) {
                t.finished();
                calcMaxRemaining();
                return EXACT;
            }
        }
        return INVALID;
    }

    private void calcMaxRemaining() {
        int min = 100000;
        for (Tower t: towers) {
            if (min > t.number) {
                min = t.number;
            }
        }
        maxRemaining = Constants.LIMIT_OF_TOWERS - min;
    }

    public int getMaxFreeSpace() {
        int maxFreeSpace = 0;
        for (Tower t: towers) {
            int freeSpace = t.limit - t.number;
            if (freeSpace > maxFreeSpace) {
                maxFreeSpace = freeSpace;
            }
        }
        return maxFreeSpace;
    }

    public void stateGameover() {
        for (Tower t: towers) {
            t.stateGameover();
        }
    }

    void update(float delta) {
        for (Tower tower: towers) {
            tower.update(delta);
        }
    }
    void render() {

        renderIndicatorLines();
        renderTowersDividers();
        for (Tower tower: towers) {
            tower.render();
        }

    }
    private void renderIndicatorLines() {

        GameScreen.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        GameScreen.shapeRenderer.setColor(Color.valueOf(Constants.DIVIDERS_COLOR));
        for (int i=0; i<1;i++){//Constants.LIMIT_OF_TOWERS; i++) {
            float y = i * height / Constants.LIMIT_OF_TOWERS + startingY;
            GameScreen.shapeRenderer.rectLine(dividersVertices.get(0), Math.round(y), dividersVertices.get(dividersVertices.size()-1), Math.round(y), 0.5f);
        }
        GameScreen.shapeRenderer.end();
    }

    private void renderTowersDividers() {
        GameScreen.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        GameScreen.shapeRenderer.setColor(Color.valueOf(Constants.DIVIDERS_COLOR));
        for (int i = 0; i< dividersVertices.size(); i++) {
            GameScreen.shapeRenderer.rect(dividersVertices.get(i) , startingY, dividerWidth, height);
        }
        GameScreen.shapeRenderer.rect(0, startingY, GameScreen.width, dividerWidth);
        GameScreen.shapeRenderer.end();

    }

}
