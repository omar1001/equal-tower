package website.quan42.equaltowers;

final class Constants {
    static final String GAME_BACKGROUND_COLOR = "FFFFD5";
    static final String DIVIDERS_COLOR = "4C4C4C";
    static final String TOWERS_COLOR = "66ccff";
    static final String TOWERS_NUMBER_COLOR = "0000ff";
    static final String NUMBERS_COLOR = "000000";
    static final String FRAME_COLOR = "ffffff";
    static final String TOWERS_CLICK_VALID = "ADFFAD";
    static final String TOWERS_CLICK_INVALID = "FF7F7F";
    static final String TOWERS_SCORE_COLOR = "ffffff";
    static final String RESTART_GAME_PANEL_COLOR = "000000";

    static final float NUMBERS_HEIGHT_RATIO = 0.25f; //upper (score, time, numbers) part height.
    static final float TOWERS_HEIGHT_RATIO = 0.75f; //lower (towers) part height.

    static final short NUMBER_OF_TOWERS = 5; //how many towers.
    static final short LIMIT_OF_TOWERS = 10; //TOWERS LIMIT
    static final short LIMIT_OF_RANDOM_TOWER_HEIGHT = 15;
    static final short TOWER_INITIAL_RANDOM_LIMIT = 5;
    static final float DIVIDER_RATIO_TO_TOWER_WIDTH = 0.1f;

    static final int TOWERS_NUMBER_FONT_SIZE = 50;
    static final int NUMBERS_SCORE_FONT_SIZE = 45;
    static final int TOWERS_SCORE_FONT_SIZE = 75;
    static final int GAMEOVER_TEXT_FONT_SIZE = 145;
    static final int RESTART_BUTTON_TEXT_FONT_SIZE = 100;


    static final float NUMBER_FRAME_RATIO_TO_NUMBERS_HEIGHT = 0.5f;
    static final int PRIMARY_NUMBER_FONT_SIZE = 75;
    static final int NUMBER_OF_NUMBERS = 3;
    static final float NUMBER_INITIAL_ALPHA = 0.25f;

    static final int SCORE_INCREMENT_VALUE = 1;

}
