package website.quan42.equaltowers;

import static sun.jvm.hotspot.debugger.win32.coff.DebugVC50X86RegisterEnums.TAG;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;


public class RestartGamePanel extends GameObject{

    private Color color;
    private GameObject gameOverText;
    private GameObject restartText;

    RestartGamePanel() {
        super();
        // Body
        color = new Color(Color.valueOf(Constants.RESTART_GAME_PANEL_COLOR));
        color.a = 0.2f;
        position.x = (GameScreen.width/16f);
        position.y = (GameScreen.height/4f);
        size.x = GameScreen.width - (GameScreen.width/8f);
        size.y = GameScreen.height - (GameScreen.height/2f);

        // GameOver text
        gameOverText = new GameObject();
        GlyphLayout layout;
        layout = new GlyphLayout(GameScreen.gameOverFont,  "GameOver");
        gameOverText.position.x = (GameScreen.width/2f) - (layout.width/2f);
        gameOverText.position.y = position.y + ((0.75f) * size.y);

        // Restart button text
        restartText = new GameObject();
        layout = new GlyphLayout(GameScreen.restartFont,  "Restart");
        restartText.position.x = (GameScreen.width/2f) - (layout.width/2f);
        restartText.position.y = position.y + ((0.25f) * size.y);
    }

    void update(float delta) {

    }

    void render() {
        // Body
        GameScreen.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        GameScreen.shapeRenderer.setColor(color);
        GameScreen.shapeRenderer.rect(
                position.x,
                position.y,
                size.x,
                size.y
        );

        GameScreen.shapeRenderer.end();

        // Text
        ETGame.batch.begin();
        GameScreen.gameOverFont.draw(
                ETGame.batch,
                "GameOver",
                gameOverText.position.x,
                gameOverText.position.y
        );
        GameScreen.restartFont.draw(
                ETGame.batch,
                "Restart",
                restartText.position.x,
                restartText.position.y
        );
        ETGame.batch.end();
    }

    public boolean isClickInsideRestartArea(int x, int y) {
        if (y > position.y && y < (position.y + size.y/2f)) {
            return x > position.x && x < position.x + size.x;
        }
        return false;
    }
}
