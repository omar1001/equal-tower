package website.quan42.equaltowers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;


class Tower extends GameObject {
    int number;
    int limit;
    private Vector2 numberPosition;
    private String numberText;
    private GlyphLayout fontLayout;
    private GameObject click;
    private GameObject plusOne;

    private Vector2 towerHeightTextV;
    private Color tempColor;

    private float startingY;

    Tower init(float center, float width, float startingY) {
        this.startingY = startingY;
        limit = (int)(Math.random() * Constants.LIMIT_OF_RANDOM_TOWER_HEIGHT + 5);
        //limit = 10;
        towerHeightTextV = new Vector2();

        position = new Vector2(center - width/2, 0);
        size = new Vector2 (width, 0);
        color = Color.valueOf(Constants.TOWERS_COLOR);

        fontLayout = new GlyphLayout();

        click = new GameObject();//dummy invisible, visible for click animation
        click.color = new Color(Color.valueOf(Constants.GAME_BACKGROUND_COLOR));
        plusOne = new GameObject();//dummy invisible, visible for adding score
        plusOne.color = new Color(Color.valueOf(Constants.TOWERS_SCORE_COLOR));
        plusOne.color.a = 0f;
        fontLayout.setText(GameScreen.towersScoreFont, "+" + Constants.SCORE_INCREMENT_VALUE);
        plusOne.position.x = center - fontLayout.width/2;
        plusOne.position.y = TowersPart.height/2 +fontLayout.height/2;

        fontLayout.setText(GameScreen.towersNumberFont, limit + "");
        towerHeightTextV.x = center - fontLayout.width/2;
        towerHeightTextV.y = GameScreen.height - fontLayout.height;



        this.number = (int) (Math.random() * Constants.TOWER_INITIAL_RANDOM_LIMIT);
        updateHeight();
        numberPosition = new Vector2();
        animateClick(true);



        return this;
    }
    void animateInvalidClick() {
        animateClick(false);
    }

    public void stateGameover() {
        // Change remaining part (color) to red
        // animateClick will not reset it because
        click.color = new Color(Color.valueOf(Constants.TOWERS_CLICK_INVALID));
    }

    private void animateClick(boolean valid) {
        // return to avoid resetting red color of no remaining move
        if (GameScreen.gameover) {
            return;
        }
        ETGame.tweenManager.killTarget(click);
        Color target;
        Color background = Color.valueOf(Constants.GAME_BACKGROUND_COLOR);
        if (valid) {
            target = Color.valueOf(Constants.TOWERS_CLICK_VALID);

        } else {
            target = Color.valueOf(Constants.TOWERS_CLICK_INVALID);
        }
        Timeline.createSequence()
                .push(
                        Tween.to(click, TweenAccessor.COLOR, 0.25f)
                                .target(target.r, target.g, target.b, target.a)
                )
                .push(
                        Tween.to(click, TweenAccessor.COLOR, 0.25f)
                                .target(background.r, background.g, background.b, background.a)
                )
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        if (type == TweenCallback.COMPLETE) {
                            // Check if gameover happened then red color the remaining
                            // because gameover change all synchronously but tween is async
                            if (GameScreen.gameover) {
                                click.color = new Color(Color.valueOf(Constants.TOWERS_CLICK_INVALID));
                            }
                        }
                    }
                })
                .start(ETGame.tweenManager);


    }

    void increaseNumber(int n) {
        this.number = n;
        animateClick(true);
        updateHeight();
    }

    void finished() {
        GameScreen.doParticles(towerHeightTextV.x, towerHeightTextV.y);
        this.number = 0;
        animateClick(true);
        //first animate tower to max height
        //then animate back to zero
        ETGame.tweenManager.killTarget(plusOne);
        ETGame.tweenManager.killTarget(this);
        // update the tower to the max height, then +1 appear, disappear, reset tower to zero
        Timeline.createSequence()
                .push(
                        Tween.to(this, TweenAccessor.SIZE, 0.5f)
                            .target(this.size.x, TowersPart.height)
                )
                .push(
                        Tween.to(plusOne, TweenAccessor.COLOR, 0.25f)
                            .target(plusOne.color.r, plusOne.color.g, plusOne.color.b, 1f)
                )
                .push(
                        Tween.to(plusOne, TweenAccessor.COLOR, 0.25f)
                            .target(plusOne.color.r, plusOne.color.g, plusOne.color.b, 0f)
                )
                .push(
                        Tween.to(this, TweenAccessor.SIZE, 0.5f)
                            .target(this.size.x, 0)
                )
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        if (type == TweenCallback.COMPLETE) {
                            limit = (int)(Math.random() * Constants.LIMIT_OF_RANDOM_TOWER_HEIGHT + 5);
                        }
                    }
                })
                .start(ETGame.tweenManager);


    }


    private void updateHeight() {
        float height = (number * TowersPart.height / limit);
        animateTower(new Vector2(size.x, height)); // size.x to make it unchangeable
    }
    private void animateTower(Vector2 target) {
        // kill current tween - or pre-existing
        ETGame.tweenManager.killTarget(this);

        // move
        Tween.to(this, TweenAccessor.SIZE, 0.5f)
                .target(target.x, target.y)
                //.ease(TweenEquations.easeOutElastic)
                .start(ETGame.tweenManager);
    }


    void update(float delta) {
        numberText = Math.round(size.y * limit / TowersPart.height) + "";
        fontLayout.setText(GameScreen.towersNumberFont, numberText);
        numberPosition.x = position.x + (size.x - fontLayout.width) / 2f;
        numberPosition.y = size.y - fontLayout.height;
    }

    void render() {
        GameScreen.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        //tower
        GameScreen.shapeRenderer.setColor(color);
        GameScreen.shapeRenderer.rect(
                position.x,
                position.y + startingY,
                size.x,
                size.y
        );
        //click animation
        GameScreen.shapeRenderer.setColor(click.color);
        GameScreen.shapeRenderer.rect(
                position.x,
                size.y + startingY,
                size.x,
                TowersPart.height-size.y
        );
        GameScreen.shapeRenderer.end();



        ETGame.batch.begin();
        tempColor = GameScreen.towersNumberFont.getColor();
        tempColor.a = 0.25f;
        GameScreen.towersNumberFont.setColor(tempColor);
        GameScreen.towersNumberFont.draw(
                ETGame.batch,
                limit +"",
                towerHeightTextV.x,
                towerHeightTextV.y
        );

        if (!numberText.equals("0")) {
            tempColor.a = 1f;
            GameScreen.towersNumberFont.setColor(tempColor);
            GameScreen.towersNumberFont.draw(
                    ETGame.batch,
                    numberText,
                    numberPosition.x,
                    numberPosition.y + startingY
            );
        }

        GameScreen.towersScoreFont.setColor(plusOne.color);
        GameScreen.towersScoreFont.draw(
                ETGame.batch,
                "+"+Constants.SCORE_INCREMENT_VALUE,
                plusOne.position.x,
                plusOne.position.y + startingY
        );
        ETGame.batch.end();

    }
}
