package website.quan42.equaltowers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;

import static website.quan42.equaltowers.ETGame.prefs;

class NumbersPart {

    float height;
    private ArrayList<Number> numbers;
    private float numberWidth;
    private float frameY;
    private float frameHeight;
    private GameObject scoreText;
    private GameObject highScoreText;
    private int score;
    private float scoreTextAlphaRef;
    private int minRemaining = 1000000;
    private boolean highscoreFlag;
    private int highScore;

    private int currentNumberIndex;


    NumbersPart init(float height) {
        minRemaining = 1000000;
        this.height = height;
        frameHeight = height * Constants.NUMBER_FRAME_RATIO_TO_NUMBERS_HEIGHT;
        frameY = frameHeight / 2f;// = (GameScreen.height - height/2f) - frameHeight/2f;
        highscoreFlag = false;
        highScore = prefs.getInteger("highScore", 0);

        scoreText = new GameObject();
        highScoreText = new GameObject();
        scoreText.position.y = ((3f * frameHeight) / 2f) + (frameHeight / 4f); // center between frame top and towers bot.
        scoreText.position.x = frameHeight / 4f;
        scoreTextAlphaRef = 0.5f;
        scoreText.color = GameScreen.numbersScoreFont.getColor();
        scoreText.color.a = scoreTextAlphaRef;
        highScoreText.color = GameScreen.numbersScoreFont.getColor();
        highScoreText.color.a = scoreTextAlphaRef;

        numbers = new ArrayList<Number>();
        numberWidth = GameScreen.width / Constants.NUMBER_OF_NUMBERS;
        GlyphLayout layout;
        for (int i=0; i<Constants.NUMBER_OF_NUMBERS; i++) {
            Number n = new Number();
            n.centerX = i * numberWidth + numberWidth/2f;
            n.number = (int)(Math.random() * (Constants.LIMIT_OF_TOWERS - Constants.TOWER_INITIAL_RANDOM_LIMIT) + 1f);
            layout = new GlyphLayout(GameScreen.numbersFont, n.number + "");
            n.position.y = frameY + frameHeight/2f + layout.height/2f;
            updateNumberXCenter(n);
            n.color = Color.valueOf(Constants.NUMBERS_COLOR);
            n.color.a = Constants.NUMBER_INITIAL_ALPHA;
            numbers.add(n);
            if (minRemaining > n.number) minRemaining = n.number;
        }
        layout = new GlyphLayout(GameScreen.towersNumberFont,  "Score  " + 10);
        scoreText.position.y += (layout.height / 2f);
        scoreText.size.x = layout.width;
        scoreText.position.x = (GameScreen.width/2f) - (layout.width/2f);

        highScoreText.position.x = (GameScreen.width - (GameScreen.width/4f));
        highScoreText.position.y = scoreText.position.y;

        // default selected current number is the first number.
        currentNumberIndex = 0;
        numbers.get(currentNumberIndex).color.a = 1f;
        score = 0;

        return this;
    }
    private void updateNumberXCenter(Number n) {
        GlyphLayout layout = new GlyphLayout(GameScreen.numbersFont, n.number + "");
        n.position.x = n.centerX - layout.width/2f;
    }
    int getCurrentNumber() {
        return numbers.get(currentNumberIndex).number;
    }

    public int getMinNumber() {
        int minNumber = 10000000;
        for (Number n: numbers) {
            if (minNumber > n.number) {
                minNumber = n.number;
            }
        }
        return minNumber;
    }
    void nextNumber() { // score +1
        Number n = numbers.get(currentNumberIndex);
        n.number = (int)(Math.random() * (Constants.LIMIT_OF_TOWERS - 1) + 1f);
        minRemaining = 1000000;
        for (Number nn: numbers) {
            if (minRemaining > nn.number) minRemaining = nn.number;
        }
        updateNumberXCenter(n);
        n.color.a = 0f;

        ETGame.tweenManager.killTarget(n);
        Tween.to(n, TweenAccessor.COLOR, 0.25f)
                .target(n.color.r, n.color.g, n.color.b, 1f)
                .start(ETGame.tweenManager);

        score++;
        prefs.putInteger("score", score);
        prefs.flush();

        if (score > highScore) { // new highscore
            highScore = score;
            prefs.putInteger("highScore", highScore);
            prefs.flush();
            if (!highscoreFlag) {
                highscoreFlag = true;
                GameScreen.doParticles(scoreText.position.x + (scoreText.size.x/2), scoreText.position.y);
            }
        }



        ETGame.tweenManager.killTarget(scoreText);
        // animate score text alpha
        Timeline.createSequence()
                .push(
                        Tween.to(scoreText, TweenAccessor.COLOR, 0.5f)
                                .target(scoreText.color.r, scoreText.color.g, scoreText.color.b, 1f)
                )
                .push(
                        Tween.to(scoreText, TweenAccessor.COLOR, 0.5f)
                                .target(scoreText.color.r, scoreText.color.g, scoreText.color.b, scoreTextAlphaRef)
                ).start( ETGame.tweenManager );

    }

    void handleClick(float x) {
        Number currN = numbers.get(currentNumberIndex);
        float widthD2 = numberWidth/2f;
        // if click on the current selected number : return
        if (x < (currN.centerX + widthD2) && x > (currN.centerX - widthD2)){
            return;
        }
        //search for the click for which number and select it the current
        for (int i=0; i<numbers.size(); i++) {
            Number n = numbers.get(i);
            if (x < (n.centerX + widthD2) && x > (n.centerX - widthD2)) {
                numbers.get(currentNumberIndex).color.a = Constants.NUMBER_INITIAL_ALPHA;
                currentNumberIndex = i;
                //animate select
                ETGame.tweenManager.killTarget(n);
                Tween.to(n, TweenAccessor.COLOR, 0.25f)
                        .target(n.color.r, n.color.g, n.color.b, 1f)
                        .start(ETGame.tweenManager);
                break;
            }
        }
    }


    void update(float delta) {

    }

    void render() {
        renderFrame();

        //render numbers;
        ETGame.batch.begin();
        for (Number n: numbers) {
            GameScreen.numbersFont.setColor(n.color);
            GameScreen.numbersFont.draw(ETGame.batch, n.number+"", n.position.x, n.position.y);
        }

        GameScreen.numbersScoreFont.setColor(scoreText.color);
        GameScreen.numbersScoreFont.draw(ETGame.batch, "Score   " + score + (highscoreFlag?"*":""), scoreText.position.x, scoreText.position.y);
        GameScreen.numbersScoreFont.draw(ETGame.batch, "HS:" + highScore, highScoreText.position.x, highScoreText.position.y);

        ETGame.batch.end();

    }
    private void renderFrame() {
        GameScreen.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        GameScreen.shapeRenderer.setColor(Color.valueOf(Constants.FRAME_COLOR));
        GameScreen.shapeRenderer.rect(0f, frameY, GameScreen.width, frameHeight);

        GameScreen.shapeRenderer.end();
    }


    class Number extends GameObject {
        int number;
        float centerX; //number center reference for width update.

    }
}
