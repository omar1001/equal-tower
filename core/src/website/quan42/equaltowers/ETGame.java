package website.quan42.equaltowers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;


public class ETGame extends Game {

    static TweenManager tweenManager;
    static Preferences prefs;
    private GameScreen gameScreen;
    private HomeScreen homeScreen;
    static SpriteBatch batch;
    ParticleEffect pe;
    @Override
    public void create() {
        batch = new SpriteBatch();
        //for animation
        tweenManager = new TweenManager();
        Tween.setCombinedAttributesLimit(4);// default is 3, yet
        // for rgba color setting
        //we need to raise to 4!
        Tween.registerAccessor(GameObject.class, new
                TweenAccessor());

         prefs = Gdx.app.getPreferences("EqualTowersPrefs");


        Gdx.app.setLogLevel(Application.LOG_DEBUG);



        gameScreen = new GameScreen(this);

        setScreen(gameScreen);


    }

    void reopenGame() {
        if (gameScreen != null) {
            gameScreen.dispose();
            gameScreen = new GameScreen(this);
        }
        homeScreen = new HomeScreen(this);
        setScreen(gameScreen);
    }

    void startGame() {
        if (homeScreen != null){
            homeScreen.dispose();
        }
        gameScreen = new GameScreen(this);
        setScreen(gameScreen);
    }



    @Override
    public void dispose() {
        gameScreen.dispose();
        batch.dispose();

    }
}
