package website.quan42.equaltowers;

import com.badlogic.gdx.graphics.Color;

class TweenAccessor implements aurelienribon.tweenengine.TweenAccessor<GameObject> {

    public static final int POSITION = 0;
    public static final int SIZE = 1;
    public static final int COLOR = 2;
    @Override
    public int getValues(GameObject gameObject, int i, float[] floats) {
        switch (i) {
            case POSITION:
                floats[0] = gameObject.position.x;
                floats[1] = gameObject.position.y;
                return 2;
            case SIZE:
                floats[0] = gameObject.size.x;
                floats[1] = gameObject.size.y;
                return 2;
            case COLOR:
                floats[0] = gameObject.color.r;
                floats[1] = gameObject.color.g;
                floats[2] = gameObject.color.b;
                floats[3] = gameObject.color.a;
                return 4;
            default:
                return -1;

        }
    }

    @Override
    public void setValues(GameObject gameObject, int i, float[] floats) {
        switch (i) {
            case POSITION:
                gameObject.position.x = floats[0];
                gameObject.position.y = floats[1];
                break;
            case SIZE:
                gameObject.size.x = floats[0];
                gameObject.size.y = floats[1];
                break;
            case COLOR:
                /*
                gameObject.color.r = floats[0];
                gameObject.color.g = floats[1];
                gameObject.color.b = floats[2];
                gameObject.color.a = floats[3];
                */
                gameObject.color = new Color(floats[0], floats[1], floats[2], floats[3]);
                break;

        }
    }
}
