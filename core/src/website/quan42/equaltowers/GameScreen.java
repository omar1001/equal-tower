package website.quan42.equaltowers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;



class GameScreen extends ScreenAdapter implements InputProcessor {

    static Viewport viewport;
    static ShapeRenderer shapeRenderer;
    static Color background;

    static float width;
    static float height;
    ETGame game;

    private TowersPart towers;
    private NumbersPart numbers;
    public static boolean gameover = true;

    static BitmapFont towersNumberFont;
    static BitmapFont numbersFont;
    static BitmapFont towersScoreFont;
    static BitmapFont numbersScoreFont;
    static BitmapFont gameOverFont;
    static BitmapFont restartFont;

    static ParticleEffect pe;
    static boolean flag = false;

    private RestartGamePanel restartGamePanel;

    GameScreen(ETGame game) {
        this.game = game;
    }
    @Override
    public void dispose() {
        shapeRenderer.dispose();
        towersNumberFont.dispose();
    }

    @Override
    public void show() {
        //gameover = false;
        viewport = new ScreenViewport();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);

        background = new Color(Color.valueOf(Constants.GAME_BACKGROUND_COLOR));

        initFonts();
        Gdx.input.setInputProcessor(this);

        pe = new ParticleEffect();
        pe.load(Gdx.files.internal("effects/flame"),Gdx.files.internal(""));

    }
    static void doParticles(float x, float y) {
        pe.getEmitters().first().setPosition(x,y);
        pe.start();
        flag = true;
    }

    private void initFonts() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/carbon.black.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();

        //towers height number font
        params.size = Constants.TOWERS_NUMBER_FONT_SIZE;
        params.color = Color.valueOf(Constants.TOWERS_NUMBER_COLOR);
        params.color.a = 0.5f;
        params.borderColor = Color.GOLD;
        params.borderColor.a = 0.75f;
        params.borderWidth = 2f;
        towersNumberFont = generator.generateFont(params);

        //towers score +1 font
        params = new FreeTypeFontGenerator.FreeTypeFontParameter();
        params.size = Constants.TOWERS_SCORE_FONT_SIZE;
        towersScoreFont = generator.generateFont(params);

        // numbers part font
        params = new FreeTypeFontGenerator.FreeTypeFontParameter();
        params.size = Constants.PRIMARY_NUMBER_FONT_SIZE;
        params.color = Color.valueOf(Constants.NUMBERS_COLOR);
        //params.borderColor = Color.WHITE;
        //params.borderWidth = 1f;
        numbersFont = generator.generateFont(params);

        // GameOver text font
        params.size = Constants.GAMEOVER_TEXT_FONT_SIZE;
        params.color = Color.RED;
        params.color.a = 0.5f;
        gameOverFont = generator.generateFont(params);

        // Restart button font
        params.size = Constants.RESTART_BUTTON_TEXT_FONT_SIZE;
        params.color = Color.WHITE;
        params.color.a = 0.75f;
        restartFont = generator.generateFont(params);

        //towers height number font
        params.size = Constants.NUMBERS_SCORE_FONT_SIZE;
        params.color = Color.BLUE;//Color.valueOf(Constants.TOWERS_NUMBER_COLOR);
        params.color.a = 0.25f;
        params.borderColor = Color.BLACK;
        params.borderColor.a = 0.25f;
        params.borderWidth = 2f;
        numbersScoreFont = generator.generateFont(params);
    }

    @Override
    public void render(float delta) {
        ETGame.tweenManager.update(delta);
        update(delta);
        render();
    }

    private void update(float delta) {

        towers.update(delta);
        numbers.update(delta);
        if (flag) {
            pe.update(delta);
        }

        if (gameover) {
            restartGamePanel.update(delta);
        }
    }

    private void render() {
        viewport.apply();
        Gdx.gl.glClearColor(
                background.r,
                background.g,
                background.b,
                0.5f
        );

        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);

        towers.render();
        numbers.render();
        if (gameover) {
            restartGamePanel.render();
        }

        pe.draw(ETGame.batch);

        if (pe.isComplete()){
            //pe.reset();
            flag = false;
        }


    }

    @Override
    public void hide() {
        shapeRenderer.dispose();
        super.hide();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height, true);
        init();
    }

    private void init() {
        gameover = false;
        width = viewport.getScreenWidth();
        height = viewport.getScreenHeight();

        towers = null;
        numbers = null;

        // Initialize othere here so that gamescreen set width & height.
        towers = new TowersPart().init(height * Constants.TOWERS_HEIGHT_RATIO);
        numbers = new NumbersPart().init(height * Constants.NUMBERS_HEIGHT_RATIO);
        restartGamePanel = new RestartGamePanel();

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenY = Math.round(height - screenY);
        if (!gameover) {
            if (screenY <= numbers.height) {
                numbers.handleClick(screenX);
            } else if (towers.checkClick(screenX, numbers.getCurrentNumber()) != TowersPart.INVALID){
                numbers.nextNumber();

                // Gameover if no remaining valid play
                if (numbers.getMinNumber() > towers.getMaxFreeSpace()) {
                    stateGameover();
                }
            }
        } else {
            if (restartGamePanel.isClickInsideRestartArea(screenX, screenY)) {
                this.init();
            }
        }
        return true;
    }

    private void stateGameover() {
        gameover = true;
        towers.stateGameover();
        // TODO: start post gameover actions like interstitials
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
