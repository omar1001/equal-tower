package website.quan42.equaltowers;

import com.badlogic.gdx.ScreenAdapter;

class HomeScreen extends ScreenAdapter {

    ETGame game;
    HomeScreen(ETGame game) {
        this.game = game;
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
