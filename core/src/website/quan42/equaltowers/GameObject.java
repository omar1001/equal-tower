package website.quan42.equaltowers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

class GameObject {
    // this class is to be extended by any object that will
    // be have position and color, that will be animated by
    // " tween engine :) "
    Vector2 position;
    Vector2 size;
    Color color;
    GameObject () {
        position = new Vector2();
        size = new Vector2();
        color = new Color();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
